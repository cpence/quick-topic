#!/usr/bin/env python
import argparse
from quick_topic import corpus_args, model_args, documents
from tqdm import tqdm

parser = argparse.ArgumentParser(
    description="Print out the top documents for all topics"
)
corpus_args.add(parser)
model_args.add(parser)
documents.add_args(parser)
parser.add_argument(
    "--num-docs",
    "-n",
    type=int,
    default=5,
    help="number of documents to print for each topic [default: 5]",
    metavar="N",
)
args = parser.parse_args()

corpus_filenames = corpus_args.load_corpus_filenames(parser, args, required=True)
lda = model_args.load(parser, args, required=True)
document_topics = model_args.load_document_topics(parser, args, required=True)

for i in tqdm(range(lda.num_topics)):
    doc_topic_list = []
    for j, doc in enumerate(document_topics):
        for pair in doc:
            if pair[0] == i:
                doc_topic_list.append((j, pair[1]))
                break

    ranked = sorted(doc_topic_list, key=lambda tup: tup[1], reverse=True)

    print(f"TOPIC {i}:")
    for j, item in enumerate(ranked):
        doc_num = item[0]
        doc_pr = item[1]

        doc_str = documents.string_for(parser, args, doc_num)
        doc_str += f" [Pr: {doc_pr}]"

        print(doc_str)

        if j == args.num_docs - 1:
            break
    print("")

#!/usr/bin/env python
# Print out the top documents for all topics in each time segment.

import os
import sys
import pickle
import gensim
import dtmmodel
import json
from dateutil.parser import isoparse
from tqdm import tqdm

# Make sure we have our arguments
if len(sys.argv) < 2:
    print("ERROR: docs-for-topics <modelfile.model> [num-docs=5]")
    quit()

model = sys.argv[1]
if len(sys.argv) >= 3:
    num_docs = int(sys.argv[2])
else:
    num_docs = 5

# Make sure we have the model file available
if not os.path.exists(model):
    print("ERROR: Specified model does not exist")
    quit()

# Make sure we have the corpus stuff available
if not os.path.exists("corpus_filenames.pickle") or not os.path.exists(
    "corpus_matrix.mm"
):
    print("ERROR: Must be run from directory containing parsed corpus")
    quit()

corpus_matrix = gensim.corpora.MmCorpus("corpus_matrix.mm")
with open("corpus_filenames.pickle", "rb") as f:
    corpus_filenames = pickle.load(f)
with open("time_slice.pickle", "rb") as f:
    time_slice = pickle.load(f)
with open("time_slice_years.pickle", "rb") as f:
    time_slice_years = pickle.load(f)
    num_time_slice = len(time_slice_years)

lda = dtmmodel.DtmModel.load(model)

# Get document-topic representation from the model
corpus_topics = lda.gamma_

for yidx, year in tqdm(enumerate(time_slice_years)):
    print("at year %d:" % (year))
    for tidx in tqdm(range(lda.num_topics)):
        time_slice_start = 0
        for i in range(yidx):
            time_slice_start += time_slice[i]
        time_slice_end = time_slice_start + time_slice[yidx]

        doc_topic_list = []
        for j, doc in enumerate(corpus_topics):
            # This is painfully inefficient, and I just don't care
            if j < time_slice_start or j >= time_slice_end:
                continue

            doc_topic_list.append((j, doc[tidx]))

        ranked = sorted(doc_topic_list, key=lambda tup: tup[1], reverse=True)

        print(f"TOPIC {tidx}:")
        for j, item in enumerate(ranked):
            doc_num = item[0]
            doc_pr = item[1]

            doc_fn = corpus_filenames[doc_num]
            with open(doc_fn) as f:
                data = json.load(f)
                doi = data.get("doi")
                title = data.get("title")
                date = data.get("date")
                dt = isoparse(date)

                print(f"    - “{title}” ({dt.year}), doi: {doi} [Pr: {doc_pr}]")
                if j == num_docs:
                    break
        print("")

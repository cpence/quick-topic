import pickle
from os import path

from gensim import corpora

CORPUS_FILENAMES = "_filenames.pickle"
CORPUS_DATES = "_dates.pickle"
CORPUS_TOKENS = "_tokens.pickle"
CORPUS_DICT = "_dict.dict"
CORPUS_MATRIX = "_matrix.mm"
CORPUS_EMBEDDINGS = "_embedding.pickle"


# Add the argument needed to find our generated corpus files.
def add(parser):
    parser.add_argument(
        "--corpus",
        "-c",
        default=".",
        help="path to generated corpus files [default: .]",
        metavar="PATH",
    )


# Load the corpus components given the parsed arguments. If required is True,
# then raise a fatal error if the corpus cannot be found at the given path.
def load_corpus_filenames(parser, args, required=False):
    filename = args.corpus + CORPUS_FILENAMES
    if not path.exists(filename):
        if required:
            parser.exit(
                1,
                f"{CORPUS_FILENAMES} not found at the specified path; run build-corpus",
            )
        return None

    with open(filename, "rb") as f:
        return pickle.load(f)


def load_corpus_dates(parser, args, required=False):
    filename = args.corpus + CORPUS_DATES
    if not path.exists(filename):
        if required:
            parser.exit(
                1,
                f"{CORPUS_DATES} not found at the specified path; run build-corpus",
            )
        return None

    with open(filename, "rb") as f:
        return pickle.load(f)


def load_corpus_tokens(parser, args, required=False):
    filename = args.corpus + CORPUS_TOKENS
    if not path.exists(filename):
        if required:
            parser.exit(
                1,
                f"{CORPUS_TOKENS} not found at the specified path; run build-corpus",
            )
        return None

    with open(filename, "rb") as f:
        return pickle.load(f)


def load_corpus_dict(parser, args, required=False):
    filename = args.corpus + CORPUS_DICT
    if not path.exists(filename):
        if required:
            parser.exit(
                1,
                f"{CORPUS_DICT} not found at the specified path; run build-corpus",
            )
        return None

    return corpora.Dictionary.load(filename)


def load_corpus_matrix(parser, args, required=False):
    filename = args.corpus + CORPUS_MATRIX
    if not path.exists(filename):
        if required:
            parser.exit(
                1,
                f"{CORPUS_MATRIX} not found at the specified path; run build-corpus",
            )
        return None

    return corpora.MmCorpus(filename)


def load_corpus_embeddings(parser, args, required=False):
    filename = args.corpus + CORPUS_EMBEDDINGS
    if not path.exists(filename):
        if required:
            parser.exit(
                1,
                f"{CORPUS_MATRIX} not found at the specified path; run build-corpus",
            )
        return None

    with open(filename, "rb") as f:
        return pickle.load(f)

from os import path
from gensim.models.ldamodel import LdaModel
import pickle

MODEL_DT = "document_topics.pickle"


def model_filename(size):
    return f"model-{size}.gensim"


def add(parser):
    parser.add_argument(
        "--model",
        "-m",
        required=True,
        help="path to trained topic model",
        metavar="PATH",
    )


def load(parser, args, required=False):
    if not path.exists(args.model):
        if required:
            parser.exit(1, f"model {args.model} not found; run build-models")
        return None

    return LdaModel.load(args.model)


def load_document_topics(parser, args, required=False):
    dir = path.dirname(args.model)
    filename = path.join(dir, MODEL_DT)
    if not path.exists(filename):
        if required:
            parser.exit(
                1,
                f"{MODEL_DT} not found in same directory as model; run build-document-topics",
            )
        return None

    with open(filename, "rb") as f:
        return pickle.load(f)

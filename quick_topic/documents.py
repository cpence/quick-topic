from quick_topic import corpus_args
import json
from dateutil.parser import isoparse


# Add an argument for printing out a customizable field from each document.
def add_args(parser):
    parser.add_argument(
        "--field",
        "-f",
        default="citation",
        choices=["citation", "filename", "doi", "authors", "title", "date", "year"],
        help="field to print for each top document [default: citation]",
        metavar="FIELD",
    )


documents_corpus_filenames = None


# Return the string corresponding to the given document.
def string_for(parser, args, doc_num):
    global documents_corpus_filenames

    if documents_corpus_filenames is None:
        documents_corpus_filenames = corpus_args.load_corpus_filenames(
            parser, args, required=True
        )

    filename = documents_corpus_filenames[doc_num]
    if hasattr(args, "field") and args.field == "filename":
        return filename

    with open(filename) as f:
        data = json.load(f)
        authors = []
        if data.get("authors") is not None:
            for au in data.get("authors"):
                authors.append(au.get("name"))
        authors = "; ".join(authors)

        doi = data.get("doi") or ""
        title = data.get("title") or ""
        date = data.get("date") or ""
        dt = isoparse(date)

    if not hasattr(args, "field") or args.field == "citation":
        return f"{authors} ({dt.year}). “{title}” doi: {doi}"
    elif args.field == "doi":
        return doi
    elif args.field == "authors":
        return authors
    elif args.field == "title":
        return title
    elif args.field == "date" or args.field == "year":
        return str(dt.year)

#!/usr/bin/env python
import argparse
from quick_topic import corpus_args, model_args
from tqdm import tqdm
from os import path
import pickle

parser = argparse.ArgumentParser(
    description="Create and save the document-by-topic matrix for future analysis"
)
corpus_args.add(parser)
model_args.add(parser)
args = parser.parse_args()

corpus_matrix = corpus_args.load_corpus_matrix(parser, args, required=True)
lda = model_args.load(parser, args, required=True)

# Crunch documents to topic representations
document_topic = []
for doc in tqdm(corpus_matrix):
    document_topic.append(lda.get_document_topics(doc))

# Save it
dir = path.dirname(args.model)
filename = path.join(dir, model_args.MODEL_DT)
with open(filename, "wb") as f:
    pickle.dump(document_topic, f)
